﻿using Microsoft.AspNetCore.Mvc;
using NovelLibrary.DataAccess;

namespace NovelWeb.Controllers
{
    public class ChapterController : Controller
    {
        ProjectLnContext context = new ProjectLnContext();
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ReadChapter(int id, int? index)
        {
            
            var currentChapter = context.Chapters.Where(c => c.Id == id).FirstOrDefault();
            int countIndex = context.Chapters.Where(c => c.NovelId == currentChapter.NovelId).ToList().Count;
            ViewBag.CountIndex = countIndex;
            if (index != null)
            {
                var currentNovel = context.Novels.Where(ln => ln.Id == currentChapter.NovelId).FirstOrDefault();
                currentChapter = context.Chapters.Where(c => c.NovelId == currentNovel.Id && c.Indexs == index).FirstOrDefault();
            } 
            var chapterImg = context.ChapterImages.Where(c => c.ChapterId == id).FirstOrDefault();
            var img = context.Images.Where(i => i.Id == chapterImg.ImageId).FirstOrDefault();
            ViewBag.Image = img;
            return View(currentChapter);
        }
    }
}
