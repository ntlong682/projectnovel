﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NovelLibrary.DataAccess;
using NovelLibrary.DataAccess.DTO;

namespace NovelWeb.Controllers
{
    public class AuthenController : Controller
    {
        ProjectLnContext context = new ProjectLnContext();

        [HttpGet]
        public async Task<IActionResult> Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(User loginUser)
        {
            if (loginUser != null)
            {
                string username = loginUser.Username;
                string password = EncryptPassword(loginUser.Password);
                var user = await context.Users.Where(u => u.Username.Equals(username) && u.Password.Equals(password)).FirstOrDefaultAsync();
                if (user != null)
                {
                    if (user.Status == false)
                    {
                        ViewBag.ErrorMessage = "Your account has been ban!";
                    }
                    else
                    {
                        HttpContext.Session.SetInt32("UserId", user.Id);
                        HttpContext.Session.SetInt32("isLogin", 1);
                        return RedirectToAction("Index", "Novels", new { area = "" });
                    }

                }
                else
                {
                    ViewBag.ErrorMessage = "Username or password invalid!";
                }
            }
            else
            {
                return View();
            }
            return View();

        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Login");
        }

        [HttpGet]
        public async Task<IActionResult> Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterUserDTO user)
        {
            if (ModelState.IsValid)
            {
                if (user != null)
                {
                    var checkExistUser = context.Users.Where(cu => cu.Username.Equals(user.UserName)).FirstOrDefault();
                    if (checkExistUser == null)
                    {
                        if (user.Images == null || user.Images.Count > 1)
                        {
                            ViewBag.ErrorMessage = "Register failed!";
                            return View();
                        }
                        else
                        {
                            if (user.Password.Equals(user.RePassword))
                            {

                                User newUser = new User
                                {
                                    Username = user.UserName,
                                    Password = EncryptPassword(user.Password),
                                    Role = false,
                                    Status = true,
                                    Avatar = ConvertToBytes(user.Images.ElementAt(0))
                                };
                                try
                                {
                                    context.Users.Add(newUser);
                                    await context.SaveChangesAsync();
                                    return RedirectToAction("Login");
                                }
                                catch (Exception ex)
                                {
                                    ViewBag.ErrorMessage = "Register failed!";
                                    return View();
                                }
                            }
                            else
                            {
                                return View();
                            }
                        }

                    }
                    else
                    {
                        ViewBag.ErrorMessage = "Register failed!";
                        return View();
                    }
                } else
                {
                    ViewBag.ErrorMessage = "Register failed!";
                    return View();
                }
            }
            else
            {
                return View();
            }

        }

        private byte[] ConvertToBytes(IFormFile file)
        {
            Stream stream = file.OpenReadStream();
            using (var memoryStream = new MemoryStream())
            {
                stream.CopyTo(memoryStream);
                return memoryStream.ToArray();
            }
        }

        private string EncryptPassword(string password)
        {
            string result = "";
            try
            {
                byte[] enCryptByte = new byte[password.Length];
                enCryptByte = System.Text.Encoding.UTF8.GetBytes(password);
                result = Convert.ToBase64String(enCryptByte);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return result;
        }
    }
}
