﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NovelLibrary.DataAccess;
using NovelLibrary.DataAccess.DTO;

namespace NovelWeb.Controllers
{
    public class NovelsController : Controller
    {
        ProjectLnContext context = new ProjectLnContext();
        // GET: NovelsController
        public ActionResult Index(int? categoryId, string? searchText)
        {

            List<Novel> listNovel;
            if (categoryId == null)
            {
                listNovel = context.Novels.Where(ln => ln.Status == true).ToList();
            }
            else
            {
                listNovel = context.Novels.Where(ln => ln.Status == true && ln.CategoryId == categoryId).ToList();
            }
            if (searchText != null && searchText.Trim().Length > 0)
            {
                listNovel = context.Novels.Where(ln => ln.Status == true && ln.Name.ToLower().Contains(searchText.ToLower())).ToList();
            }

            var listCategories = context.Categories.ToList();
            List<NovelShowDTO> lsResult = new List<NovelShowDTO>();
            foreach (var item in listNovel)
            {
                var NovelImg = context.NovelImages.Where(n => n.NovelId == item.Id).FirstOrDefault();
                var img = context.Images.Where(i => i.Id == NovelImg.ImageId).FirstOrDefault();
                NovelShowDTO temp = new NovelShowDTO
                {
                    Id = item.Id,
                    Author = item.Author,
                    Name = item.Name,
                    CategoryId = item.CategoryId,
                    CategoryName = context.Categories.Where(c => c.Id == item.CategoryId).FirstOrDefault().Name,
                    Descripton = item.Descripton,
                    ImageData = img.ImageData
                };
                lsResult.Add(temp);
            }
            ViewBag.CategoryList = listCategories;
            return View(lsResult);
        }

        [HttpPost]
        public ActionResult Index(string searchText)
        {

            return RedirectToAction("Index", new { searchText = searchText });
        }

        // GET: NovelsController/Details/5
        public ActionResult Details(int id)
        {
            var novel = context.Novels.Where(n => n.Id == id).FirstOrDefault();
            var NovelImg = context.NovelImages.Where(n => n.NovelId == novel.Id).FirstOrDefault();
            var img = context.Images.Where(i => i.Id == NovelImg.ImageId).FirstOrDefault();
            ViewBag.CoverImg = img;
            var lsChapter = context.Chapters.Where(c => c.NovelId == novel.Id).ToList();
            ViewBag.ChapterList = lsChapter;
            ViewBag.CategoryName = context.Categories.Where(c => c.Id == novel.CategoryId).FirstOrDefault().Name;
            var NovelComment = context.NovelComments.Where(c => c.NovelId == id).ToList();
            List<UserCommentDTO> lsComment = new List<UserCommentDTO>();
            if(NovelComment.Count > 0)
            {
                foreach (var item in NovelComment)
                {
                    int userId = item.UserId;
                    var userCmt = context.Users.Where(u => u.Id == userId).FirstOrDefault();
                    UserCommentDTO temp = new UserCommentDTO
                    {
                        Avatar = userCmt.Avatar,
                        UserName = userCmt.Username,
                        Comments = item.Contents,
                        Date = item.Date
                    };
                    lsComment.Add(temp);
                }
            }
            ViewBag.CommentList = lsComment;
            return View(novel);
        }

        [HttpPost]
        public ActionResult CommentNovel(string comment, int userId, int novelId)
        {
            NovelComment newCmt = new NovelComment
            {
                UserId = userId,
                Contents = comment,
                NovelId = novelId,
                Date = DateTime.Now
            };
            context.NovelComments.Add(newCmt);
            context.SaveChanges();
            return RedirectToAction("Details", new { id = novelId });
        }

        // GET: NovelsController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: NovelsController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: NovelsController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: NovelsController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: NovelsController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: NovelsController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
