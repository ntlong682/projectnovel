﻿using System;
using System.Collections.Generic;

namespace NovelLibrary.DataAccess;

public partial class ChapterImage
{
    public int Id { get; set; }

    public int ImageId { get; set; }

    public int ChapterId { get; set; }

    public virtual Chapter Chapter { get; set; } = null!;

    public virtual Image Image { get; set; } = null!;
}
