﻿using System;
using System.Collections.Generic;

namespace NovelLibrary.DataAccess;

public partial class Category
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public virtual ICollection<Novel> Novels { get; } = new List<Novel>();
}
