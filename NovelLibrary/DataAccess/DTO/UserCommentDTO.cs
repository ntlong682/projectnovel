﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NovelLibrary.DataAccess.DTO
{
    public class UserCommentDTO
    {
        public byte[] Avatar { get; set; }
        public string UserName { get; set; }
        public string Comments { get; set; }
        public DateTime Date { get; set; }
    }
}
