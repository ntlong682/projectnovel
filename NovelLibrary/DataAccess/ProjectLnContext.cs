﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace NovelLibrary.DataAccess;

public partial class ProjectLnContext : DbContext
{
    public ProjectLnContext()
    {
    }

    public ProjectLnContext(DbContextOptions<ProjectLnContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Category> Categories { get; set; }

    public virtual DbSet<Chapter> Chapters { get; set; }

    public virtual DbSet<ChapterComment> ChapterComments { get; set; }

    public virtual DbSet<ChapterImage> ChapterImages { get; set; }

    public virtual DbSet<Image> Images { get; set; }

    public virtual DbSet<Novel> Novels { get; set; }

    public virtual DbSet<NovelComment> NovelComments { get; set; }

    public virtual DbSet<NovelImage> NovelImages { get; set; }

    public virtual DbSet<User> Users { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=(local);uid=sa;pwd=123;database=ProjectLN;Integrated security = true;TrustServerCertificate=True;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Category>(entity =>
        {
            entity.ToTable("Category");
        });

        modelBuilder.Entity<Chapter>(entity =>
        {
            entity.ToTable("Chapter");

            entity.HasOne(d => d.Novel).WithMany(p => p.Chapters)
                .HasForeignKey(d => d.NovelId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Chapter_Novel");
        });

        modelBuilder.Entity<ChapterComment>(entity =>
        {
            entity.ToTable("ChapterComment");

            entity.Property(e => e.Date).HasColumnType("datetime");

            entity.HasOne(d => d.Chapter).WithMany(p => p.ChapterComments)
                .HasForeignKey(d => d.ChapterId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ChapterComment_ChapterComment");

            entity.HasOne(d => d.User).WithMany(p => p.ChapterComments)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ChapterComment_User");
        });

        modelBuilder.Entity<ChapterImage>(entity =>
        {
            entity.ToTable("ChapterImage");

            entity.HasOne(d => d.Chapter).WithMany(p => p.ChapterImages)
                .HasForeignKey(d => d.ChapterId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ChapterImage_Chapter");

            entity.HasOne(d => d.Image).WithMany(p => p.ChapterImages)
                .HasForeignKey(d => d.ImageId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ChapterImage_Image");
        });

        modelBuilder.Entity<Image>(entity =>
        {
            entity.ToTable("Image");

            entity.Property(e => e.ImageData).HasColumnType("image");
        });

        modelBuilder.Entity<Novel>(entity =>
        {
            entity.ToTable("Novel");

            entity.HasOne(d => d.Category).WithMany(p => p.Novels)
                .HasForeignKey(d => d.CategoryId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Novel_Category");
        });

        modelBuilder.Entity<NovelComment>(entity =>
        {
            entity.ToTable("NovelComment");

            entity.Property(e => e.Date).HasColumnType("datetime");

            entity.HasOne(d => d.Novel).WithMany(p => p.NovelComments)
                .HasForeignKey(d => d.NovelId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_NovelComment_Novel");

            entity.HasOne(d => d.User).WithMany(p => p.NovelComments)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_NovelComment_User");
        });

        modelBuilder.Entity<NovelImage>(entity =>
        {
            entity.ToTable("NovelImage");

            entity.HasOne(d => d.Image).WithMany(p => p.NovelImages)
                .HasForeignKey(d => d.ImageId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_NovelImage_Image");

            entity.HasOne(d => d.Novel).WithMany(p => p.NovelImages)
                .HasForeignKey(d => d.NovelId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_NovelImage_Novel");
        });

        modelBuilder.Entity<User>(entity =>
        {
            entity.ToTable("User");

            entity.Property(e => e.Avatar).HasColumnType("image");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
