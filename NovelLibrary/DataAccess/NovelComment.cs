﻿using System;
using System.Collections.Generic;

namespace NovelLibrary.DataAccess;

public partial class NovelComment
{
    public int Id { get; set; }

    public int UserId { get; set; }

    public int NovelId { get; set; }

    public string Contents { get; set; } = null!;

    public DateTime Date { get; set; }

    public virtual Novel Novel { get; set; } = null!;

    public virtual User User { get; set; } = null!;
}
