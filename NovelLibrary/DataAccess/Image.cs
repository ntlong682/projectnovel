﻿using System;
using System.Collections.Generic;

namespace NovelLibrary.DataAccess;

public partial class Image
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public byte[] ImageData { get; set; } = null!;

    public virtual ICollection<ChapterImage> ChapterImages { get; } = new List<ChapterImage>();

    public virtual ICollection<NovelImage> NovelImages { get; } = new List<NovelImage>();
}
