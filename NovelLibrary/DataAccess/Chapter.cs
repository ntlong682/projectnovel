﻿using System;
using System.Collections.Generic;

namespace NovelLibrary.DataAccess;

public partial class Chapter
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public int Indexs { get; set; }

    public int NovelId { get; set; }

    public string Contents { get; set; }

    public virtual ICollection<ChapterComment> ChapterComments { get; } = new List<ChapterComment>();

    public virtual ICollection<ChapterImage> ChapterImages { get; } = new List<ChapterImage>();

    public virtual Novel Novel { get; set; } = null!;
}
