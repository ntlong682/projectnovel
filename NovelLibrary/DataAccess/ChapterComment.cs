﻿using System;
using System.Collections.Generic;

namespace NovelLibrary.DataAccess;

public partial class ChapterComment
{
    public int Id { get; set; }

    public int UserId { get; set; }

    public int ChapterId { get; set; }

    public string? Contents { get; set; }

    public DateTime? Date { get; set; }

    public virtual Chapter Chapter { get; set; } = null!;

    public virtual User User { get; set; } = null!;
}
