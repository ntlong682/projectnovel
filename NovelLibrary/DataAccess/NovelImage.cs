﻿using System;
using System.Collections.Generic;

namespace NovelLibrary.DataAccess;

public partial class NovelImage
{
    public int Id { get; set; }

    public int ImageId { get; set; }

    public int NovelId { get; set; }

    public virtual Image Image { get; set; } = null!;

    public virtual Novel Novel { get; set; } = null!;
}
