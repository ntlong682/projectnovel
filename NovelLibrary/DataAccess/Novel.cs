﻿using System;
using System.Collections.Generic;

namespace NovelLibrary.DataAccess;

public partial class Novel
{
    public int Id { get; set; }

    public string Name { get; set; } = null!;

    public string Author { get; set; } = null!;

    public bool Status { get; set; }

    public string Descripton { get; set; } = null!;

    public int CategoryId { get; set; }

    public virtual Category Category { get; set; } = null!;

    public virtual ICollection<Chapter> Chapters { get; } = new List<Chapter>();

    public virtual ICollection<NovelComment> NovelComments { get; } = new List<NovelComment>();

    public virtual ICollection<NovelImage> NovelImages { get; } = new List<NovelImage>();
}
