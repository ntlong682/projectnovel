﻿using System;
using System.Collections.Generic;

namespace NovelLibrary.DataAccess;

public partial class User
{
    public int Id { get; set; }

    public string Username { get; set; } = null!;

    public string Password { get; set; } = null!;

    public bool Status { get; set; }

    public bool Role { get; set; }

    public byte[] Avatar { get; set; } = null!;

    public virtual ICollection<ChapterComment> ChapterComments { get; } = new List<ChapterComment>();

    public virtual ICollection<NovelComment> NovelComments { get; } = new List<NovelComment>();
}
