﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NovelLibrary.Utils
{
    public class Utils
    {
        public static string EncryptPassword(string password)
        {
            string result = "";
            try
            {
                byte[] enCryptByte = new byte[password.Length];
                enCryptByte = System.Text.Encoding.UTF8.GetBytes(password);
                result = Convert.ToBase64String(enCryptByte);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return result;
        }

        public static byte[] ConvertFileToByte(string imgPath)
        {
            byte[] imageData = null;
            FileStream stream = new FileStream(imgPath, FileMode.Open, FileAccess.Read);
            BinaryReader binaryReader = new BinaryReader(stream);
            imageData = binaryReader.ReadBytes((int)stream.Length);
            return imageData;
        }

        public static int ConvertStringToInt(string str)
        {
            try
            {
                return Convert.ToInt32(str);
            } catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
