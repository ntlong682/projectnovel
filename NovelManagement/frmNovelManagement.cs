﻿using Microsoft.EntityFrameworkCore;
using NovelLibrary.DataAccess;
using NovelLibrary.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NovelManagement
{
    public partial class frmNovelManagement : Form
    {
        ProjectLnContext dbcontext = new ProjectLnContext();
        BindingSource source;
        string imgPath = "";
        int NovelId = 0;
        bool checkEditImage = false;
        public frmNovelManagement()
        {
            InitializeComponent();
            LoadCategory();
            LoadNovelList();
            btnEditChapter.Enabled = false;
            btnEditComments.Enabled = false;
            btnEdit.Enabled = false;
        }

        public void LoadNovelList()
        {
            var novels = dbcontext.Novels.ToList();
            try
            {
                source = new BindingSource();
                source.DataSource = novels;
                dgvNovelList.DataSource = null;
                dgvNovelList.DataSource = source;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Load novel list failed");
            }
        }

        public void LoadCategory()
        {
            var categories = dbcontext.Categories.ToList();
            cboCategory.DataSource = categories;
            cboCategory.DisplayMember = "Name";
            cboCategory.ValueMember = "Id";
            cboCategory.SelectedIndex = 0;
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            ClearText();
        }

        private void btnEditChapter_Click(object sender, EventArgs e)
        {
            var novel = dbcontext.Novels.Where(n => n.Id == NovelId).FirstOrDefault();
            frmChapterManagement frmChapterManagement = new frmChapterManagement(novel);
            frmChapterManagement.Show();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmDashboard dashboard = new frmDashboard();
            dashboard.Show();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string name = txtName.Text;
            string author = txtAuthor.Text;
            string description = txtDescription.Text;
            int CategoryId = Utils.ConvertStringToInt(cboCategory.SelectedValue.ToString());
            if (name != null && name.Trim().Length > 0 && author != null && author.Trim().Length > 0
                && description != null && description.Trim().Length > 0)
            {
                if (imgPath.Length == 0)
                {
                    imgPath = "H:\\WORK\\Nam4-Ki4\\PRN211\\Project\\ProjectNovel\\NovelManagement\\Images\\defaultCoverBook.jpg";
                }
                NovelLibrary.DataAccess.Image image = new NovelLibrary.DataAccess.Image
                {
                    Name = name + "CoverImage",
                    ImageData = Utils.ConvertFileToByte(imgPath)
                };
                try
                {
                    dbcontext.Images.Add(image);
                    dbcontext.SaveChanges();
                    Novel newNovel = new Novel
                    {
                        Name = name,
                        Author = author,
                        CategoryId = CategoryId,
                        Status = cboxStatus.Checked,
                        Descripton = description
                    };
                    dbcontext.Novels.Add(newNovel);
                    dbcontext.SaveChanges();
                    NovelImage ni = new NovelImage
                    {
                        ImageId = image.Id,
                        NovelId = newNovel.Id
                    };
                    dbcontext.NovelImages.Add(ni);
                    dbcontext.SaveChanges();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error", "Add novel  failed");
                }

            }
            else
            {
                MessageBox.Show("Error", "Invalid Novel");
            }
            LoadNovelList();
            ClearText();

        }

        public void ClearText()
        {
            checkEditImage = false;
            txtId.Text = "";
            txtName.Text = "";
            txtAuthor.Text = "";
            txtDescription.Text = "";
            cboxStatus.Checked = false;
            cboCategory.SelectedIndex = 0;
            pboxCoverImage.Image = null;
            pboxCoverImage.Update();
            btnEditChapter.Enabled = false;
            btnEditComments.Enabled = false;
            btnEdit.Enabled = false;
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            checkEditImage = true;
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "png files(*.png)|*.png|jpg files(*.jpg)|*.jpg|All files(*.*)|*.*";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                imgPath = dialog.FileName.ToString();
                pboxCoverImage.ImageLocation = imgPath;
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            var novel = dbcontext.Novels.Where(n => n.Id == NovelId).FirstOrDefault();
            if (novel != null)
            {
                string name = txtName.Text;
                string author = txtAuthor.Text;
                string description = txtDescription.Text;
                if (name != null && name.Trim().Length > 0 && author != null && author.Trim().Length > 0
                && description != null && description.Trim().Length > 0)
                {
                    if (imgPath.Length == 0)
                    {
                        imgPath = "H:\\WORK\\Nam4-Ki4\\PRN211\\Project\\ProjectNovel\\NovelManagement\\Images\\defaultCoverBook.jpg";
                    }
                    try
                    {
                        var novelImage = dbcontext.NovelImages.Where(i => i.NovelId == NovelId).FirstOrDefault();
                        if (checkEditImage == true)
                        {
                            var img = dbcontext.Images.Where(i => i.Id == novelImage.ImageId).FirstOrDefault();
                            img.ImageData = Utils.ConvertFileToByte(imgPath);
                            dbcontext.Images.Update(img);
                        }
                        

                        novel.Name = name;
                        novel.Author = author;
                        novel.Descripton = description;
                        novel.Status = cboxStatus.Checked;
                        novel.CategoryId = Utils.ConvertStringToInt(cboCategory.SelectedValue.ToString());
                        dbcontext.Novels.Update(novel);
                        dbcontext.SaveChanges();

                    } catch (Exception ex)
                    {
                        MessageBox.Show("Error", "Invalid Novel");
                    }
                    
                } else
                {
                    MessageBox.Show("Error", "Invalid Novel");
                }
            }
            LoadNovelList();
            ClearText();

        }

        private void dgvNovelList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            int count = dbcontext.Novels.ToList().Count;
            if (index < count && index >= 0)
            {
                DataGridViewRow selectedRow = dgvNovelList.Rows[index];
                int id = Int32.Parse(selectedRow.Cells[0].Value.ToString());
                var novel = dbcontext.Novels.Where(x => x.Id == id).FirstOrDefault();
                if(novel != null)
                {
                    NovelId = id;
                    txtId.Text = id.ToString();
                    txtAuthor.Text = novel.Author;
                    txtName.Text = novel.Name;
                    txtDescription.Text = novel.Descripton;

                    int categoryId = novel.CategoryId;
                    var cate = dbcontext.Categories.Where(c => c.Id == categoryId).FirstOrDefault();

                    cboCategory.Text = cate.Name;
                    cboxStatus.Checked = novel.Status;

                    var novelImage = dbcontext.NovelImages.Where(ni => ni.NovelId == id).FirstOrDefault();
                    var img = dbcontext.Images.Where(i => i.Id == novelImage.ImageId).FirstOrDefault();
                    pboxCoverImage.Image = ConvertByteToImage(img.ImageData);
                    pboxCoverImage.Update();

                    btnEdit.Enabled = true;
                    btnEditChapter.Enabled = true;
                    btnEditComments.Enabled = true;
                }
               
            }
        }

        private System.Drawing.Image ConvertByteToImage(byte[] imgData)
        {
            MemoryStream ms = new MemoryStream(imgData);
            return System.Drawing.Image.FromStream(ms);
        }

        private void btnEditComments_Click(object sender, EventArgs e)
        {
            frmNovelComments frmNovel = new frmNovelComments(NovelId);
            if(frmNovel.ShowDialog() == DialogResult.OK)
            {

            }
        }
    }
}
