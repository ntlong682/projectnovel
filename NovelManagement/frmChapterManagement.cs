﻿using Microsoft.EntityFrameworkCore;
using NovelLibrary.DataAccess;
using NovelLibrary.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Net.Mime.MediaTypeNames;

namespace NovelManagement
{

    public partial class frmChapterManagement : Form
    {
        ProjectLnContext dbcontext = new ProjectLnContext();
        BindingSource source;
        string imgPath = "";
        Novel curentNovel;
        Chapter currentChapter;
        string currentChapterContent;
        bool checkEditImage = false;
        public frmChapterManagement(Novel novel)
        {
            InitializeComponent();
            btnEdit.Enabled = false;
            curentNovel = novel;
            LoadChapterList();
        }

        public frmChapterManagement(Novel novel, Chapter chapter)
        {
            InitializeComponent();
            if(chapter.Id == 0)
            {
                btnEdit.Enabled = false;
            } else
            {
                btnEdit.Enabled = true;
                var chapterImg = dbcontext.ChapterImages.Where(c => c.ChapterId == chapter.Id).FirstOrDefault();
                var img = dbcontext.Images.Where(i => i.Id == chapterImg.ImageId).FirstOrDefault();
                pboxCoverImage.Image = ConvertByteToImage(img.ImageData);
                pboxCoverImage.Update();
            }
            currentChapter = chapter;
            curentNovel = novel;
            currentChapterContent = chapter.Contents;
            LoadChapterList();
            txtId.Text = chapter.Id.ToString();
            txtName.Text = chapter.Name;
            txtNovelName.Text = novel.Name;
            txtIndex.Value = chapter.Indexs;
            
        }

        public void LoadChapterList()
        {
            var chapters = dbcontext.Chapters.Where(c => c.NovelId == curentNovel.Id).OrderBy(c => c.Indexs).ToList();
            try
            {
                source = new BindingSource();
                source.DataSource = chapters;
                dgvChapterList.DataSource = null;
                dgvChapterList.DataSource = source;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Load chapter list failed");
            }
        }

        private void btnContents_Click(object sender, EventArgs e)
        {
            if (currentChapter != null && curentNovel != null)
            {
                frmChapterContents form = new frmChapterContents(curentNovel, currentChapter);
                this.Hide();
                form.Show();

            }
            else if (currentChapter == null && curentNovel != null)
            {
                currentChapter = new Chapter
                {
                    Name = txtName.Text,
                    Indexs = (int)txtIndex.Value,
                    NovelId = curentNovel.Id
                };
                frmChapterContents form = new frmChapterContents(curentNovel, currentChapter);
                this.Hide();
                form.Show();
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            ClearText();
        }

        public void ClearText()
        {
            checkEditImage = false;
            btnEdit.Enabled = false;
            currentChapter = null;
            currentChapterContent = "";
            txtId.Text = "";
            txtName.Text = "";
            txtNovelName.Text = curentNovel.Name;
            txtIndex.Value = 0;
            pboxCoverImage.Image = null;
            pboxCoverImage.Update();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string name = txtName.Text;
            string author = txtNovelName.Text;
            int index = Utils.ConvertStringToInt(txtIndex.Text);
            if (name != null && name.Trim().Length > 0 && author != null && author.Trim().Length > 0
                && currentChapterContent != null && currentChapterContent.Trim().Length > 0)
            {
                var checkChapterIndex = dbcontext.Chapters.Where(c => c.NovelId == curentNovel.Id && c.Indexs == index).FirstOrDefault();
                if (checkChapterIndex == null)
                {
                    if (imgPath.Length == 0)
                    {
                        imgPath = "H:\\WORK\\Nam4-Ki4\\PRN211\\Project\\ProjectNovel\\NovelManagement\\Images\\defaultCoverBook.jpg";
                    }
                    try
                    {
                        NovelLibrary.DataAccess.Image image = new NovelLibrary.DataAccess.Image
                        {
                            Name = name + "CoverImage",
                            ImageData = Utils.ConvertFileToByte(imgPath)
                        };
                        dbcontext.Images.Add(image);
                        dbcontext.SaveChanges();
                        //currentChapterContent = currentChapterContent.Replace("\n", "<br>");
                        Chapter chapter = new Chapter
                        {
                            Name = name,
                            Indexs = index,
                            NovelId = curentNovel.Id,
                            Contents = currentChapterContent
                        };
                        dbcontext.Chapters.Add(chapter);
                        dbcontext.SaveChanges();
                        ChapterImage chapterImage = new ChapterImage
                        {
                            ChapterId = chapter.Id,
                            ImageId = image.Id
                        };
                        dbcontext.ChapterImages.Add(chapterImage);
                        dbcontext.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error", "Add failed");
                    }

                }
                else
                {
                    MessageBox.Show("Error", "Add failed");
                }

            }
            else
            {
                MessageBox.Show("Error", "Add failed");
            }
            ClearText();
            LoadChapterList();

        }

        private void dgvChapterList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            int count = dbcontext.Novels.ToList().Count;
            if (index < count && index >= 0)
            {
                DataGridViewRow selectedRow = dgvChapterList.Rows[index];
                int id = Int32.Parse(selectedRow.Cells[0].Value.ToString());
                var chapter = dbcontext.Chapters.Where(c => c.Id == id).FirstOrDefault();
                if (chapter != null)
                {
                    currentChapter = chapter;
                    txtId.Text = id.ToString();
                    txtName.Text = chapter.Name;
                    txtIndex.Value = chapter.Indexs;
                    var chapterImg = dbcontext.ChapterImages.Where(c => c.ChapterId == id).FirstOrDefault();
                    var img = dbcontext.Images.Where(i => i.Id == chapterImg.ImageId).FirstOrDefault();
                    pboxCoverImage.Image = ConvertByteToImage(img.ImageData);
                    pboxCoverImage.Update();
                    currentChapterContent = chapter.Contents;
                    btnEdit.Enabled = true;
                }

            }
        }
        private System.Drawing.Image ConvertByteToImage(byte[] imgData)
        {
            MemoryStream ms = new MemoryStream(imgData);
            return System.Drawing.Image.FromStream(ms);
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "png files(*.png)|*.png|jpg files(*.jpg)|*.jpg|All files(*.*)|*.*";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                imgPath = dialog.FileName.ToString();
                pboxCoverImage.ImageLocation = imgPath;
                checkEditImage = true;
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (currentChapter != null)
            {
                string name = txtName.Text;
                int index = Utils.ConvertStringToInt(txtIndex.Text);
                if (name != null && name.Trim().Length > 0 && currentChapterContent != null && currentChapterContent.Trim().Length > 0)
                {
                    var checkChapterIndex = dbcontext.Chapters.Where(c => c.NovelId == curentNovel.Id && c.Indexs == index).FirstOrDefault();
                    if (checkChapterIndex == null  || (checkChapterIndex.Id == currentChapter.Id && checkChapterIndex.Indexs == currentChapter.Indexs))
                    {
                        if (imgPath.Length == 0)
                        {
                            imgPath = "H:\\WORK\\Nam4-Ki4\\PRN211\\Project\\ProjectNovel\\NovelManagement\\Images\\defaultCoverBook.jpg";
                        }
                        try
                        {
                            var chapterImg = dbcontext.ChapterImages.Where(ci => ci.ChapterId == currentChapter.Id).FirstOrDefault();
                            if(checkEditImage == true)
                            {
                                var img = dbcontext.Images.Where(i => i.Id == chapterImg.ImageId).FirstOrDefault();
                                img.ImageData = Utils.ConvertFileToByte(imgPath);
                                dbcontext.Images.Update(img);
                                dbcontext.SaveChanges();
                            }
                            var editChapter = dbcontext.Chapters.Where(ec => ec.Id == currentChapter.Id).FirstOrDefault();
                            editChapter.Name = name;
                            //currentChapterContent = currentChapterContent.Replace("\n", "<br>");
                            editChapter.Contents = currentChapterContent;
                            editChapter.Indexs = (int)txtIndex.Value;
                            dbcontext.Chapters.Update(editChapter);
                            dbcontext.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Error", "Add failed");
                        }

                    }
                    else
                    {
                        MessageBox.Show("Error", "Add failed");
                    }

                }
                else
                {
                    MessageBox.Show("Error", "Add failed");
                }
            }

            ClearText();
            LoadChapterList();
        }

        private void frmChapterManagement_Load(object sender, EventArgs e)
        {
            txtNovelName.Text = curentNovel.Name;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (currentChapter != null)
            {
                var chapter = dbcontext.Chapters.Where(c => c.Id == currentChapter.Id).FirstOrDefault();
                var imgChapter = dbcontext.ChapterImages.Where(c => c.ChapterId == chapter.Id).FirstOrDefault();
                var img = dbcontext.Images.Where(i => i.Id == imgChapter.ImageId).FirstOrDefault();
                dbcontext.ChapterImages.Remove(imgChapter);
                dbcontext.SaveChanges();
                dbcontext.Images.Remove(img);
                dbcontext.SaveChanges();
                dbcontext.Chapters.Remove(chapter);
                dbcontext.SaveChanges();
            }
            ClearText();
            LoadChapterList();
        }
    }
}
