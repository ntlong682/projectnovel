﻿using Microsoft.EntityFrameworkCore;
using NovelLibrary.DataAccess;
using NovelLibrary.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NovelManagement
{
    public partial class frmCategoriesManagement : Form
    {
        ProjectLnContext context  = new ProjectLnContext();
        BindingSource source;
        public frmCategoriesManagement()
        {
            InitializeComponent();
            LoadCategoryList();
        }

        public void LoadCategoryList()
        {
            var categories = context.Categories.ToList();
            try
            {
                source = new BindingSource();
                source.DataSource = categories;
                dgvCategoryList.DataSource = null;
                dgvCategoryList.DataSource = source;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Load Categories list failed");
            }
        }


        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmDashboard dashboard = new frmDashboard();
            dashboard.Show();
        }

        private void dgvCategoryList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            int count = context.Categories.ToList().Count;
            if (index < count && index >= 0)
            {
                DataGridViewRow selectedRow = dgvCategoryList.Rows[index];
                int id = Int32.Parse(selectedRow.Cells[0].Value.ToString());
                var cate = context.Categories.FirstOrDefault(x => x.Id == id);
                txtId.Text = cate.Id.ToString();
                txtName.Text = cate.Name.ToString();

            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string name = txtName.Text;
            if(name == null || name.Length == 0)
            {
                MessageBox.Show("Error", "Name can not empty");
            } else
            {
                Category newCate = new Category
                {
                    Name = name
                };
                try
                {
                    context.Categories.Add(newCate);
                    context.SaveChanges();
                } catch (Exception ex)
                {
                    MessageBox.Show("Error", "Add Categories failed");
                }
            }
            ClearText();
            LoadCategoryList();
        }

        public void ClearText()
        {
            txtId.Text = "";
            txtName.Text = "";
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            ClearText();
            LoadCategoryList();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            int id = Utils.ConvertStringToInt(txtId.Text);
            string name = txtName.Text;
            if (name == null || name.Length == 0)
            {
                MessageBox.Show("Error", "Name can not empty");
            }
            else
            {
                var cate = context.Categories.Where(c => c.Id == id).FirstOrDefault();
                if(cate != null)
                {
                    try
                    {
                        cate.Name = name;
                        context.Categories.Update(cate);
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error", "Add Categories failed");
                    }
                } else
                {
                    MessageBox.Show("Error", "Category does not exist");
                }
                
            }
            ClearText();
            LoadCategoryList();
        }
    }
}
