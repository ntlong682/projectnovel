﻿namespace NovelManagement
{
    partial class frmNovelManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpBoxFilter = new System.Windows.Forms.GroupBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.pboxCoverImage = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnEditComments = new System.Windows.Forms.Button();
            this.cboCategory = new System.Windows.Forms.ComboBox();
            this.txtAuthor = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnEditChapter = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cboxStatus = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblid = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.dgvNovelList = new System.Windows.Forms.DataGridView();
            this.btnBack = new System.Windows.Forms.Button();
            this.grpBoxFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboxCoverImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNovelList)).BeginInit();
            this.SuspendLayout();
            // 
            // grpBoxFilter
            // 
            this.grpBoxFilter.Controls.Add(this.btnBrowse);
            this.grpBoxFilter.Controls.Add(this.pboxCoverImage);
            this.grpBoxFilter.Controls.Add(this.label6);
            this.grpBoxFilter.Controls.Add(this.btnEditComments);
            this.grpBoxFilter.Controls.Add(this.cboCategory);
            this.grpBoxFilter.Controls.Add(this.txtAuthor);
            this.grpBoxFilter.Controls.Add(this.label5);
            this.grpBoxFilter.Controls.Add(this.txtDescription);
            this.grpBoxFilter.Controls.Add(this.label3);
            this.grpBoxFilter.Controls.Add(this.btnEditChapter);
            this.grpBoxFilter.Controls.Add(this.label4);
            this.grpBoxFilter.Controls.Add(this.btnEdit);
            this.grpBoxFilter.Controls.Add(this.btnAdd);
            this.grpBoxFilter.Controls.Add(this.label2);
            this.grpBoxFilter.Controls.Add(this.cboxStatus);
            this.grpBoxFilter.Controls.Add(this.label1);
            this.grpBoxFilter.Controls.Add(this.lblid);
            this.grpBoxFilter.Controls.Add(this.txtId);
            this.grpBoxFilter.Controls.Add(this.btnRefresh);
            this.grpBoxFilter.Controls.Add(this.txtName);
            this.grpBoxFilter.Controls.Add(this.lblName);
            this.grpBoxFilter.Location = new System.Drawing.Point(660, 59);
            this.grpBoxFilter.Name = "grpBoxFilter";
            this.grpBoxFilter.Size = new System.Drawing.Size(455, 665);
            this.grpBoxFilter.TabIndex = 38;
            this.grpBoxFilter.TabStop = false;
            this.grpBoxFilter.Text = "Action";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(353, 377);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(81, 34);
            this.btnBrowse.TabIndex = 60;
            this.btnBrowse.Text = "Broswe";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // pboxCoverImage
            // 
            this.pboxCoverImage.Location = new System.Drawing.Point(154, 377);
            this.pboxCoverImage.Name = "pboxCoverImage";
            this.pboxCoverImage.Size = new System.Drawing.Size(180, 148);
            this.pboxCoverImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pboxCoverImage.TabIndex = 59;
            this.pboxCoverImage.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label6.Location = new System.Drawing.Point(29, 377);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 20);
            this.label6.TabIndex = 58;
            this.label6.Text = "Cover Image";
            // 
            // btnEditComments
            // 
            this.btnEditComments.Location = new System.Drawing.Point(318, 551);
            this.btnEditComments.Name = "btnEditComments";
            this.btnEditComments.Size = new System.Drawing.Size(116, 34);
            this.btnEditComments.TabIndex = 57;
            this.btnEditComments.Text = "Edit Comments";
            this.btnEditComments.UseVisualStyleBackColor = true;
            this.btnEditComments.Click += new System.EventHandler(this.btnEditComments_Click);
            // 
            // cboCategory
            // 
            this.cboCategory.FormattingEnabled = true;
            this.cboCategory.Location = new System.Drawing.Point(154, 315);
            this.cboCategory.Name = "cboCategory";
            this.cboCategory.Size = new System.Drawing.Size(252, 23);
            this.cboCategory.TabIndex = 56;
            // 
            // txtAuthor
            // 
            this.txtAuthor.Location = new System.Drawing.Point(154, 122);
            this.txtAuthor.Name = "txtAuthor";
            this.txtAuthor.Size = new System.Drawing.Size(252, 23);
            this.txtAuthor.TabIndex = 55;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(29, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 20);
            this.label5.TabIndex = 54;
            this.label5.Text = "Author";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(154, 175);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(252, 86);
            this.txtDescription.TabIndex = 53;
            this.txtDescription.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(29, 174);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 20);
            this.label3.TabIndex = 52;
            this.label3.Text = "Description";
            // 
            // btnEditChapter
            // 
            this.btnEditChapter.Location = new System.Drawing.Point(172, 551);
            this.btnEditChapter.Name = "btnEditChapter";
            this.btnEditChapter.Size = new System.Drawing.Size(128, 34);
            this.btnEditChapter.TabIndex = 51;
            this.btnEditChapter.Text = "Edit Chapter";
            this.btnEditChapter.UseVisualStyleBackColor = true;
            this.btnEditChapter.Click += new System.EventHandler(this.btnEditChapter_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(29, 551);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 20);
            this.label4.TabIndex = 50;
            this.label4.Text = "Contents";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(337, 607);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(97, 32);
            this.btnEdit.TabIndex = 46;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(185, 607);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(97, 32);
            this.btnAdd.TabIndex = 45;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(29, 315);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 20);
            this.label2.TabIndex = 43;
            this.label2.Text = "Category";
            // 
            // cboxStatus
            // 
            this.cboxStatus.AutoSize = true;
            this.cboxStatus.Location = new System.Drawing.Point(154, 274);
            this.cboxStatus.Name = "cboxStatus";
            this.cboxStatus.Size = new System.Drawing.Size(59, 19);
            this.cboxStatus.TabIndex = 42;
            this.cboxStatus.Text = "Active";
            this.cboxStatus.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(29, 271);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 20);
            this.label1.TabIndex = 41;
            this.label1.Text = "Status";
            // 
            // lblid
            // 
            this.lblid.AutoSize = true;
            this.lblid.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblid.Location = new System.Drawing.Point(29, 34);
            this.lblid.Name = "lblid";
            this.lblid.Size = new System.Drawing.Size(22, 20);
            this.lblid.TabIndex = 32;
            this.lblid.Text = "Id";
            // 
            // txtId
            // 
            this.txtId.Enabled = false;
            this.txtId.Location = new System.Drawing.Point(154, 34);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(137, 23);
            this.txtId.TabIndex = 30;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(29, 607);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(97, 32);
            this.btnRefresh.TabIndex = 29;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(154, 79);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(252, 23);
            this.txtName.TabIndex = 12;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblName.Location = new System.Drawing.Point(29, 78);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(49, 20);
            this.lblName.TabIndex = 11;
            this.lblName.Text = "Name";
            // 
            // dgvNovelList
            // 
            this.dgvNovelList.AllowDrop = true;
            this.dgvNovelList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNovelList.Location = new System.Drawing.Point(46, 59);
            this.dgvNovelList.Name = "dgvNovelList";
            this.dgvNovelList.RowTemplate.Height = 25;
            this.dgvNovelList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvNovelList.Size = new System.Drawing.Size(576, 703);
            this.dgvNovelList.TabIndex = 37;
            this.dgvNovelList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvNovelList_CellClick);
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(845, 730);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(97, 32);
            this.btnBack.TabIndex = 58;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // frmNovelManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1187, 796);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.grpBoxFilter);
            this.Controls.Add(this.dgvNovelList);
            this.Name = "frmNovelManagement";
            this.Text = "frmNovelManagement";
            this.grpBoxFilter.ResumeLayout(false);
            this.grpBoxFilter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboxCoverImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNovelList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupBox grpBoxFilter;
        private Button btnEditChapter;
        private Label label4;
        private Button btnEdit;
        private Button btnAdd;
        private Label label2;
        private CheckBox cboxStatus;
        private Label label1;
        private Label lblid;
        private TextBox txtId;
        private Button btnRefresh;
        private TextBox txtName;
        private Label lblName;
        private DataGridView dgvNovelList;
        private ComboBox cboCategory;
        private TextBox txtAuthor;
        private Label label5;
        private RichTextBox txtDescription;
        private Label label3;
        private Button btnEditComments;
        private Button btnBack;
        private Label label6;
        private Button btnBrowse;
        private PictureBox pboxCoverImage;
    }
}