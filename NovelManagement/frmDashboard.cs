﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NovelManagement
{
    public partial class frmDashboard : Form
    {
        public frmDashboard()
        {
            InitializeComponent();
        }

        private void btnUserManagement_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmUserManagement frmUser = new frmUserManagement();
            frmUser.Show();
        }

        private void btnNovelManagement_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmNovelManagement frmNovelManagement = new frmNovelManagement();
            frmNovelManagement.Show();
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin login = new frmLogin();
            login.Show();
        }

        private void btnCategoryManagement_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmCategoriesManagement frmCategoriesManagement = new frmCategoriesManagement();
            frmCategoriesManagement.Show();
        }
    }
}
