﻿namespace NovelManagement
{
    partial class frmNovelComments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvCommentList = new System.Windows.Forms.DataGridView();
            this.btnDelete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCommentList)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvCommentList
            // 
            this.dgvCommentList.AllowDrop = true;
            this.dgvCommentList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCommentList.Location = new System.Drawing.Point(12, 21);
            this.dgvCommentList.Name = "dgvCommentList";
            this.dgvCommentList.RowTemplate.Height = 25;
            this.dgvCommentList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCommentList.Size = new System.Drawing.Size(776, 472);
            this.dgvCommentList.TabIndex = 38;
            this.dgvCommentList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCommentList_CellClick);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(346, 509);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(97, 36);
            this.btnDelete.TabIndex = 39;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // frmNovelComments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 557);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.dgvCommentList);
            this.Name = "frmNovelComments";
            this.Text = "frmNovelComments";
            this.Load += new System.EventHandler(this.frmNovelComments_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCommentList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DataGridView dgvCommentList;
        private Button btnDelete;
    }
}