﻿using NovelLibrary.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NovelManagement
{
    public partial class frmNovelComments : Form
    {

        ProjectLnContext dbcontext = new ProjectLnContext();
        BindingSource source;
        int SelectedID = 0;
        int NovelId = 0;
        NovelComment cmt;
        public frmNovelComments(int NovelId)
        {
            InitializeComponent();
            this.NovelId = NovelId;
            LoadNovelList();
        }
        public void LoadNovelList()
        {
            var comments = dbcontext.NovelComments.Where(x => x.NovelId == NovelId).OrderBy(x => x.Date).ToList();
            try
            {
                source = new BindingSource();
                source.DataSource = comments;
                dgvCommentList.DataSource = null;
                dgvCommentList.DataSource = source;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Load novel list failed");
            }
        }

        private void frmNovelComments_Load(object sender, EventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if(cmt != null)
            {
                if (MessageBox.Show($"Do you want to delete comment with id: {cmt.Id}",
                "Confirm delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    dbcontext.NovelComments.Remove(cmt);
                    dbcontext.SaveChanges();
                }
            } else
            {
                MessageBox.Show("Error", "Comment invalid");
            }
            LoadNovelList();
            
        }

        private void dgvCommentList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            int count = dbcontext.NovelComments.Where(x => x.NovelId == NovelId).ToList().Count;
            if (index < count && index >= 0)
            {
                DataGridViewRow selectedRow = dgvCommentList.Rows[index];
                int id = Int32.Parse(selectedRow.Cells[0].Value.ToString());
                cmt = dbcontext.NovelComments.Where(c => c.Id == id).FirstOrDefault();
            }
        }
    }
}
