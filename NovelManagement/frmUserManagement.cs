﻿using Microsoft.VisualBasic.ApplicationServices;
using NovelLibrary.DataAccess;
using NovelLibrary.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;
using User = NovelLibrary.DataAccess.User;

namespace NovelManagement
{
    public partial class frmUserManagement : Form
    {
        ProjectLnContext dbcontext = new ProjectLnContext();
        BindingSource source;
        string imgPath = "";
        bool checkEditImage = false;
        public frmUserManagement()
        {
            InitializeComponent();
            LoadMembersList();
        }

        public void LoadMembersList()
        {
            var members = dbcontext.Users.ToList();
            try
            {
                source = new BindingSource();
                source.DataSource = members;
                dgvMemberList.DataSource = null;
                dgvMemberList.DataSource = source;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Load member list failed");
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
           ClearText();
        }

        public void ClearText()
        {
            txtId.Text = "";
            txtName.Text = "";
            txtUserPassword.Text = "";
            txtUserPassword.ReadOnly = false;
            cboxStatus.Checked = false;
            cboxRole.Checked = false;
            pboxAvatar.Image = null;
            pboxAvatar.Update();
            checkEditImage = false;
            LoadMembersList();
        }

        private void dgvMemberList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            int count = dbcontext.Users.ToList().Count;
            if (index < count && index >= 0)
            {
                DataGridViewRow selectedRow = dgvMemberList.Rows[index];
                int id = Int32.Parse(selectedRow.Cells[0].Value.ToString());
                var user = dbcontext.Users.FirstOrDefault(x => x.Id == id);
                txtId.Text = user.Id.ToString();
                txtName.Text = user.Username;
                txtUserPassword.Text = user.Password;
                txtUserPassword.ReadOnly = true;
                cboxRole.Checked = user.Role;
                cboxStatus.Checked = user.Status;
                pboxAvatar.Image = ConvertByteToImage(user.Avatar);

            }
        }

        private System.Drawing.Image ConvertByteToImage(byte[] imgData)
        {
            MemoryStream ms = new MemoryStream(imgData);
            return System.Drawing.Image.FromStream(ms);
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "png files(*.png)|*.png|jpg files(*.jpg)|*.jpg|All files(*.*)|*.*";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                imgPath = dialog.FileName.ToString();
                pboxAvatar.ImageLocation = imgPath;
            }
            checkEditImage = true;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            int id = Utils.ConvertStringToInt(txtId.Text);
            var user = dbcontext.Users.FirstOrDefault(x => x.Id == id);
            user.Username = txtName.Text;
            user.Status = cboxStatus.Checked;
            user.Role = cboxRole.Checked;
            if (imgPath.Length == 0)
            {
                imgPath = "H:\\WORK\\Nam4-Ki4\\PRN211\\Project\\ProjectNovel\\NovelManagement\\Images\\defaultAvatar.jpg";
            }
            if(checkEditImage == true)
            {
                user.Avatar = Utils.ConvertFileToByte(imgPath);
            }
            
            try
            {
                dbcontext.Users.Update(user);
                dbcontext.SaveChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error", "Edit user failed");
            }
            LoadMembersList();
            ClearText();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string userName = txtName.Text;
            var user = dbcontext.Users.Where(u => u.Username.Equals(userName)).FirstOrDefault();
            if (user == null)
            {

                if (imgPath.Length == 0)
                {
                    imgPath = "H:\\WORK\\Nam4-Ki4\\PRN211\\Project\\ProjectNovel\\NovelManagement\\Images\\defaultAvatar.jpg";
                }
                User newUser = new User
                {
                    Username = txtName.Text,
                    Password = Utils.EncryptPassword(txtUserPassword.Text),
                    Status = cboxStatus.Checked,
                    Role = cboxRole.Checked,
                    Avatar = Utils.ConvertFileToByte(imgPath)
                };
                try
                {
                    dbcontext.Users.Add(newUser);
                    dbcontext.SaveChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                MessageBox.Show("Error", "Add user failed");
            }
            LoadMembersList();
            ClearText();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int id = Utils.ConvertStringToInt(txtId.Text);
            var user = dbcontext.Users.Where(u => u.Id == id).FirstOrDefault();
            if (user != null)
            {
                var userChapterComment = dbcontext.ChapterComments.Where(c => c.UserId == id).ToList();
                if (userChapterComment.Count > 0)
                {
                    foreach (var item in userChapterComment)
                    {
                        try
                        {
                            dbcontext.ChapterComments.Remove(item);
                        }
                        catch
                        {
                            MessageBox.Show("Error", "Delete chapter comment failed");
                        }
                    }
                }
                var userNovelComment = dbcontext.NovelComments.Where(c => c.UserId == id).ToList();
                if (userNovelComment.Count > 0)
                {
                    foreach (var item in userNovelComment)
                    {
                        try
                        {
                            dbcontext.NovelComments.Remove(item);
                        }
                        catch
                        {
                            MessageBox.Show("Error", "Delete novel comment failed");
                        }
                    }
                }
                try
                {
                    dbcontext.Users.Remove(user);
                    dbcontext.SaveChanges();
                } catch (Exception ex)
                {
                    MessageBox.Show("Error", "Delete user failed");
                }
                LoadMembersList();
                ClearText();
            } else
            {
                MessageBox.Show("Error", "User dont exist");
            }

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmDashboard frmDashboard = new frmDashboard();
            frmDashboard.Show();
        }

        private void pboxAvatar_Click(object sender, EventArgs e)
        {

        }
    }
}
