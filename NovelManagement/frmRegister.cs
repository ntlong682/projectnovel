﻿using NovelLibrary.DataAccess;
using NovelLibrary.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NovelManagement
{
    public partial class frmRegister : Form
    {
        ProjectLnContext context = new ProjectLnContext();
        private string imgPath = "";
        public frmRegister()
        {
            InitializeComponent();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            string username = txtUsername.Text;
            var tempUser = context.Users.FirstOrDefault(x => x.Username == username);
            if (tempUser == null)
            {
                string password = txtPassword.Text;
                string rePassword = txtRepassword.Text;
                if (password.Equals(rePassword) == true)
                {
                    if (imgPath.Length == 0)
                    {
                        imgPath = "H:\\WORK\\Nam4-Ki4\\PRN211\\Project\\ProjectNovel\\NovelManagement\\Images\\defaultAvatar.jpg";
                    }
                    byte[] imgData = Utils.ConvertFileToByte(imgPath);
                    if (imgData.Length > 0)
                    {

                        User newUser = new User
                        {
                            Username = username,
                            Password = Utils.EncryptPassword(password),
                            Status = true,
                            Role = true,
                            Avatar = imgData
                        };
                        try
                        {
                            context.Users.Add(newUser);
                            context.SaveChanges();
                            this.Hide();
                            frmLogin login = new frmLogin();
                            login.Show();

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Register failed", "Error");

                        }
                    }
                    else
                    {
                        MessageBox.Show("Image invalid", "Error");

                    }
                }
                else
                {
                    MessageBox.Show("Password and Re-password is not match", "Error");
                }

            }
            else
            {
                MessageBox.Show("Username already exist", "Error");
            }
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "png files(*.png)|*.png|jpg files(*.jpg)|*.jpg|All files(*.*)|*.*";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                imgPath = dialog.FileName.ToString();
                pboxAvatar.ImageLocation = imgPath;
            }
            
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void pboxAvatar_Click(object sender, EventArgs e)
        {

        }
    }
}
