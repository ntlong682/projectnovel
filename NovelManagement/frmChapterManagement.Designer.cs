﻿namespace NovelManagement
{
    partial class frmChapterManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpBoxFilter = new System.Windows.Forms.GroupBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtIndex = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.pboxCoverImage = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnEditComments = new System.Windows.Forms.Button();
            this.txtNovelName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnContents = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lblid = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.dgvChapterList = new System.Windows.Forms.DataGridView();
            this.grpBoxFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtIndex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxCoverImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvChapterList)).BeginInit();
            this.SuspendLayout();
            // 
            // grpBoxFilter
            // 
            this.grpBoxFilter.Controls.Add(this.btnDelete);
            this.grpBoxFilter.Controls.Add(this.label2);
            this.grpBoxFilter.Controls.Add(this.txtIndex);
            this.grpBoxFilter.Controls.Add(this.label1);
            this.grpBoxFilter.Controls.Add(this.btnBrowse);
            this.grpBoxFilter.Controls.Add(this.pboxCoverImage);
            this.grpBoxFilter.Controls.Add(this.label6);
            this.grpBoxFilter.Controls.Add(this.btnEditComments);
            this.grpBoxFilter.Controls.Add(this.txtNovelName);
            this.grpBoxFilter.Controls.Add(this.label5);
            this.grpBoxFilter.Controls.Add(this.btnContents);
            this.grpBoxFilter.Controls.Add(this.btnEdit);
            this.grpBoxFilter.Controls.Add(this.btnAdd);
            this.grpBoxFilter.Controls.Add(this.lblid);
            this.grpBoxFilter.Controls.Add(this.txtId);
            this.grpBoxFilter.Controls.Add(this.btnRefresh);
            this.grpBoxFilter.Controls.Add(this.txtName);
            this.grpBoxFilter.Controls.Add(this.lblName);
            this.grpBoxFilter.Location = new System.Drawing.Point(553, 12);
            this.grpBoxFilter.Name = "grpBoxFilter";
            this.grpBoxFilter.Size = new System.Drawing.Size(455, 654);
            this.grpBoxFilter.TabIndex = 40;
            this.grpBoxFilter.TabStop = false;
            this.grpBoxFilter.Text = "Action";
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(185, 606);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(97, 32);
            this.btnDelete.TabIndex = 65;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(29, 234);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 20);
            this.label2.TabIndex = 64;
            this.label2.Text = "Contents";
            // 
            // txtIndex
            // 
            this.txtIndex.Location = new System.Drawing.Point(154, 167);
            this.txtIndex.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.txtIndex.Name = "txtIndex";
            this.txtIndex.Size = new System.Drawing.Size(137, 23);
            this.txtIndex.TabIndex = 63;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(29, 165);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 20);
            this.label1.TabIndex = 61;
            this.label1.Text = "Index";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(353, 295);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(81, 34);
            this.btnBrowse.TabIndex = 60;
            this.btnBrowse.Text = "Broswe";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // pboxCoverImage
            // 
            this.pboxCoverImage.Location = new System.Drawing.Point(154, 295);
            this.pboxCoverImage.Name = "pboxCoverImage";
            this.pboxCoverImage.Size = new System.Drawing.Size(180, 148);
            this.pboxCoverImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pboxCoverImage.TabIndex = 59;
            this.pboxCoverImage.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label6.Location = new System.Drawing.Point(29, 295);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 20);
            this.label6.TabIndex = 58;
            this.label6.Text = "Chapter Image";
            // 
            // btnEditComments
            // 
            this.btnEditComments.Enabled = false;
            this.btnEditComments.Location = new System.Drawing.Point(175, 477);
            this.btnEditComments.Name = "btnEditComments";
            this.btnEditComments.Size = new System.Drawing.Size(116, 34);
            this.btnEditComments.TabIndex = 57;
            this.btnEditComments.Text = "Edit Comments";
            this.btnEditComments.UseVisualStyleBackColor = true;
            // 
            // txtNovelName
            // 
            this.txtNovelName.Location = new System.Drawing.Point(154, 122);
            this.txtNovelName.Name = "txtNovelName";
            this.txtNovelName.ReadOnly = true;
            this.txtNovelName.Size = new System.Drawing.Size(252, 23);
            this.txtNovelName.TabIndex = 55;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(29, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 20);
            this.label5.TabIndex = 54;
            this.label5.Text = "Novel Name";
            // 
            // btnContents
            // 
            this.btnContents.Location = new System.Drawing.Point(154, 228);
            this.btnContents.Name = "btnContents";
            this.btnContents.Size = new System.Drawing.Size(128, 34);
            this.btnContents.TabIndex = 51;
            this.btnContents.Text = "Contents";
            this.btnContents.UseVisualStyleBackColor = true;
            this.btnContents.Click += new System.EventHandler(this.btnContents_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(337, 537);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(97, 32);
            this.btnEdit.TabIndex = 46;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(185, 537);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(97, 32);
            this.btnAdd.TabIndex = 45;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lblid
            // 
            this.lblid.AutoSize = true;
            this.lblid.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblid.Location = new System.Drawing.Point(29, 34);
            this.lblid.Name = "lblid";
            this.lblid.Size = new System.Drawing.Size(22, 20);
            this.lblid.TabIndex = 32;
            this.lblid.Text = "Id";
            // 
            // txtId
            // 
            this.txtId.Enabled = false;
            this.txtId.Location = new System.Drawing.Point(154, 34);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(137, 23);
            this.txtId.TabIndex = 30;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(29, 537);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(97, 32);
            this.btnRefresh.TabIndex = 29;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(154, 79);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(252, 23);
            this.txtName.TabIndex = 12;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblName.Location = new System.Drawing.Point(29, 78);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(49, 20);
            this.lblName.TabIndex = 11;
            this.lblName.Text = "Name";
            // 
            // dgvChapterList
            // 
            this.dgvChapterList.AllowDrop = true;
            this.dgvChapterList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvChapterList.Location = new System.Drawing.Point(12, 12);
            this.dgvChapterList.Name = "dgvChapterList";
            this.dgvChapterList.RowTemplate.Height = 25;
            this.dgvChapterList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvChapterList.Size = new System.Drawing.Size(499, 703);
            this.dgvChapterList.TabIndex = 39;
            this.dgvChapterList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvChapterList_CellClick);
            // 
            // frmChapterManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1038, 725);
            this.Controls.Add(this.grpBoxFilter);
            this.Controls.Add(this.dgvChapterList);
            this.Name = "frmChapterManagement";
            this.Text = "frmChapterManagement";
            this.Load += new System.EventHandler(this.frmChapterManagement_Load);
            this.grpBoxFilter.ResumeLayout(false);
            this.grpBoxFilter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtIndex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pboxCoverImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvChapterList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private GroupBox grpBoxFilter;
        private Button btnBrowse;
        private PictureBox pboxCoverImage;
        private Label label6;
        private Button btnEditComments;
        private TextBox txtNovelName;
        private Label label5;
        private Button btnContents;
        private Button btnEdit;
        private Button btnAdd;
        private Label lblid;
        private TextBox txtId;
        private Button btnRefresh;
        private TextBox txtName;
        private Label lblName;
        private DataGridView dgvChapterList;
        private Label label2;
        private NumericUpDown txtIndex;
        private Label label1;
        private Button btnDelete;
    }
}