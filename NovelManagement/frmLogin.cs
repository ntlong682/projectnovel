using NovelLibrary.DataAccess;
using NovelLibrary.Utils;

namespace NovelManagement
{
    public partial class frmLogin : Form
    {
        ProjectLnContext dbContext = new ProjectLnContext();
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string username = txtUsername.Text;
            string password = Utils.EncryptPassword(txtPassword.Text);
            var user = dbContext.Users.Where(u => u.Username == username && u.Password == password).FirstOrDefault();
            if(user != null)
            {
                this.Hide();
                frmDashboard dashboard = new frmDashboard();
                dashboard.Show();
            } else
            {
                MessageBox.Show("Email or password invalid", "Error");
            }
        }

        private void btnGoToRegister_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmRegister register = new frmRegister();
            register.Show();
        }
    }
}