﻿using NovelLibrary.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NovelManagement
{
    public partial class frmChapterContents : Form
    {
        Novel novel;
        Chapter chapter;
        public frmChapterContents(Novel novel, Chapter chapter)
        {
            InitializeComponent();
            this.novel = novel;
            this.chapter = chapter;
            txtChapterContents.Text = chapter.Contents;
        }

        private void frmChapterContents_Load(object sender, EventArgs e)
        {

        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            chapter.Contents = txtChapterContents.Text;
            if(chapter.Contents.Length > 0)
            {
                frmChapterManagement frm = new frmChapterManagement(novel, chapter);
                this.Hide();
                frm.Show();
            } else
            {
                MessageBox.Show("Error", "Contents is empty");
            }
            
        }
    }
}
